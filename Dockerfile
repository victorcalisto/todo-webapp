FROM ruby:3.3.0

# Atualizar os pacotes e instalar as dependências
RUN apt-get update -y

# Instala dependencias
RUN apt-get install -qy --no-install-recommends \
    curl \
    gpg \
    autoconf  \
    bison  \
    build-essential  \
    libssl-dev  \
    libyaml-dev  \
    libreadline6-dev  \
    zlib1g-dev  \
    libncurses5-dev  \
    libffi-dev  \
    libgdbm6  \
    libgdbm-dev \
    libpq-dev \
    postgresql \ 
    postgresql-client

# Define o diretório de trabalho
WORKDIR /app

# Copia as gemas
# A versao do Rails esta fixada no Gemfile
COPY Gemfile* .

# Instala o Bundler
# Fixe a sua versao de bundle
RUN gem install bundler -v 2.5.4
RUN bundler install -j $(nproc)

# Limpar o cache do apt-get e outros arquivos temporários
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Tratamento de erros
COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]

# Sobe o Servidor
CMD ["sh", "-c", "rails server -b 0.0.0.0"]