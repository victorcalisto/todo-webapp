# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Testa todo', type: :system do
  scenario 'Cria item' do
    visit '/todos'
    fill_in 'Description', with: 'Alguma descrição'
    click_link_or_button 'Create Todo'
    expect(page).to have_content 'Alguma descrição'
  end

  scenario 'Exclui item' do
    visit '/todos'
    fill_in 'Description', with: 'Alguma descrição'
    click_link_or_button 'Create Todo'
    expect(page).to have_content 'Alguma descrição'

    click_button 'Delete'
    expect(page).not_to have_content 'Alguma descrição'
  end
end
