# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Todo, type: :model do
  context 'Valida presença da descrição' do
    it 'Quando está presente' do
      todo = Todo.create(description: 'Alguma descrição')
      expect(todo).to be_valid
    end

    it 'Quando nulo' do
      todo = Todo.create(description: nil)
      expect(todo).not_to be_valid
    end

    it 'Quando em branco' do
      todo = Todo.create(description: '   ')
      expect(todo).not_to be_valid
    end
  end
end
