# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Todos', type: :request do
  describe 'GET /index' do
    it 'Visita a pagina' do
      get todos_path
      expect(response).to have_http_status(:success)
    end

    it 'Atribui Variavel' do
      todo = Todo.create!(description: 'Nova Variavel')
      get todos_path
      expect(controller.instance_variable_get(:@todos)).to eq([todo])
    end
  end

  describe 'POST /create' do
    context 'Com parametros validos' do
      let!(:valid_params) { { todo: { description: 'Novo Todo' } } }

      it 'Cria novo Todo' do
        expect do
          post todos_path, params: valid_params
        end.to change(Todo, :count).by(1)
      end

      it 'Redirecionamento do create' do
        post todos_path, params: valid_params
        expect(response).to redirect_to(todos_path)
      end
    end

    context 'Com parametros invalidos' do
      let!(:invalid_params) { { todo: { description: '' } } }

      it 'Nao cria um novo todo' do
        expect do
          post todos_path, params: invalid_params
        end.to change(Todo, :count).by(0)
      end
    end
  end

  describe 'DELETE /destroy' do
    let!(:todo) { Todo.create!(description: 'Test Todo') }

    it 'Deleta o todo' do
      expect do
        delete todo_path(todo)
      end.to change(Todo, :count).by(-1)
    end

    it 'Redirecionamento do delete' do
      delete todo_path(todo)
      expect(response).to redirect_to(todos_path)
    end
  end
end
